<?php

use App\Http\Controllers\api\NodoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('nodo/listAll', [NodoController::class, 'index']);
Route::post('nodo/create', [NodoController::class, 'store']);
Route::get('nodo/getParents', [NodoController::class, 'getParents']);
Route::get('nodo/getChildrenByParent/{parent_id}', [NodoController::class, 'getChildrenByParent']);
Route::delete('nodo/delete/{id}', [NodoController::class, 'destroy']);


