
# Prueba Apok

Desarrollo de la prueba de backend para postular a la vacante de Desarrollador Laravel


## Requerimientos
- Laravel 9
- PHP 8.0
- Mysql o MariaDB
- Composer
- Git
## Descarga

Clonar el repositorio en la carpeta que desee

```bash
  git clone https://gitlab.com/aryelingmanosalva92/prueba-apok.git
```
    
## Variables de Entorno

Para correr la documentación de este proyecto, se necesitan la siguiente variable de entorno en el archivo .env 

`L5_SWAGGER_GENERATE_ALWAYS=true`

En caso de la base de datos:
- Crear base de datos con nombre: `prueba_apok` o el de su preferencia.
En el archivo .env configurar:

`DB_CONNECTION=mysql`

`DB_HOST=127.0.0.1`

`DB_PORT=3306`

`DB_DATABASE=prueba_apok`

`DB_USERNAME=root`

`DB_PASSWORD=`

Estos datos son de prueba y van acuerdo a la configuración de su equipo

## Paquetes y dependencias de Instalación
- Desde la carpeta del proyecto prueba_apok
- Ejecute:

```bash
  composer install 
```
- Luego ejecute las migraciones y seeder de la siguiente manera:

```bash
  php artisan migrate --seed
```

- Para generar la documentación:
```bash
  php artisan l5-swagger:generate
```

- Y por último ejecute:
```bash
  php artisan serve
```
## Documentation
En la raiz del proyecto ubique la carpeta `postman` dentro de ella encontraran los archivos: 
- `Prueba Apok.postman_collection.json` 

- `prueba_apok.postman_environment`

Estos archivos deberán ser importados para la prueba de los endpoints realizados:

Para acceder a la documentación del proyecto puede acceder a través de:

http://localhost:8000/api/documentation

