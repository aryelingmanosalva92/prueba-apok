<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Nodo;
use Illuminate\Http\Request;
use Luecano\NumeroALetras\NumeroALetras;
use \NumberFormatter;
use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="L5 OpenApi",
 *      description="Documentación de la Api Prueba Apok"
 * )
 *
 */
class NodoController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/nodo/listAll",
     *     description="Listar todos los nodos",
     *     @OA\Response(response="default", description="Welcome page",
     *          @OA\Header(
     *              header="Accept-Language",
     *              description="Idioma en formato ISO 639-1 ",
     *              @OA\Schema( type="string" ),
     *          ),
     *           @OA\Header(
     *              header="Time-Zone",
     *              description="Zona horaria ",
     *              @OA\Schema( type="string" )
     *          ),
     *      ),
     * )
     */
    public function index(Request $request)
    {
        $nodos = Nodo::all();
        $nodos_translated =$this->headerParams($request, $nodos);

        return $nodos_translated;
    }

    /**
     * @OA\Post(
     *     path="/api/nodo/create",
     *     description="Crear nodo",
     *     @OA\RequestBody(
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                  property="parent_id",
     *                  type="int",
     *                  example="3 || null"
     *             ),
     *        ),
     *
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                  property="res",
     *                  type="boolean",
     *                  example="true"
     *             ),
     *             @OA\Property(
     *                  property="msg",
     *                  type="string",
     *                  example="OK"
     *             ),
     *             @OA\Property(
     *                property="data",
     *                type="array",
     *                @OA\Items(
     *                      @OA\Property(
     *                         property="id",
     *                         type="string",
     *                         example="1"
     *                      ),
     *                      @OA\Property(
     *                         property="title",
     *                         type="string",
     *                         example="ONCE"
     *                      ),
     *                      @OA\Property(
     *                         property="parent_id",
     *                         type="number",
     *                         example="2"
     *                      ),
     *                      @OA\Property(
     *                         property="created_at",
     *                         type="string",
     *                         example="2022-11-27T01:29:15.000000Z"
     *                      ),
     *                      @OA\Property(
     *                         property="updated_at",
     *                         type="string",
     *                         example="2022-11-27T01:29:15.000000Z"
     *                      ),
     *                ),

     *             ),
     *        ),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Error",

     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                  property="res",
     *                  type="boolean",
     *                  example="false"
     *             ),
     *             @OA\Property(
     *                  property="msg",
     *                  type="string",
     *                  example="El Nodo no existe"
     *             ),
     *        ),
     *     ),
     *      @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                  property="res",
     *                  type="boolean",
     *                  example="false"
     *             ),
     *             @OA\Property(
     *                  property="msg",
     *                  type="string",
     *                  example="Error"
     *             ),
     *        ),
     *     ),
     * )
     */
    public function store(Request $request)
    {
        $formatter = new NumeroALetras();

        if (!empty($request->parent_id)) {
            $nodo_parent = Nodo::find($request->parent_id);
            if (!$nodo_parent) {
                return response()->json([
                    'res' => false,
                    'msg' => 'El padre asignado no existe'
                ], 404);
            }
        }

        try {
            $nodo = Nodo::create([
                'parent_id' => $nodo_parent->id,
                'title' => "",
            ]);
            $nodo->title = $formatter->toWords($nodo->id);
            $nodo->save();
            return response()->json([
                'res' => true,
                'msg' => 'Ok',
                'data' => $nodo
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'res' => false,
                'msg' => 'Error'
            ], 400);
        }


        return $nodo;
    }


/**
     * @OA\Delete(
     *     path="/api/nodo/delete/{id}",
     *     description="Eliminar nodos",
     *     @OA\Parameter(
     *         description="Id del nodo",
     *         in="path",
     *         name="id",
     *         required=false,
     *         @OA\Schema(type="int"),
     *
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                  property="res",
     *                  type="boolean",
     *                  example="true"
     *             ),
     *             @OA\Property(
     *                  property="msg",
     *                  type="string",
     *                  example="OK"
     *             ),     *
     *        ),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Error",

     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                  property="res",
     *                  type="boolean",
     *                  example="false"
     *             ),
     *             @OA\Property(
     *                  property="msg",
     *                  type="string",
     *                  example="El Nodo no existe"
     *             ),
     *        ),
     *     ),
     *      @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                  property="res",
     *                  type="boolean",
     *                  example="false"
     *             ),
     *             @OA\Property(
     *                  property="msg",
     *                  type="string",
     *                  example="Error"
     *             ),
     *        ),
     *     ),
     * )
     */
    public function destroy($id)
    {
        try {
            $nodo = Nodo::find($id);
            if (empty($nodo)) {
                return response()->json([
                    'res' => false,
                    'msg' => 'Nodo no existe'
                ], 404);
            }

            $nodo->delete();

            return response()->json([
                'res' => true,
                'msg' => 'Ok'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'res' => false,
                'msg' => 'Error',
                'data' => $th
            ], 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/nodo/getChildrenByParent/{parent_id}",
     *     description="Listar nodos hijos a partir del padre",
     *     @OA\Parameter(
     *         description="Id del nodo padre",
     *         in="path",
     *         name="parent_id",
     *         required=false,
     *         @OA\Schema(type="int"),
     *
     *     ),
     *     @OA\Response(
     *         @OA\Header(
     *              header="Accept-Language",
     *              description="Idioma en formato ISO 639-1 ",
     *              @OA\Schema( type="string" ),
     *         ),
     *         @OA\Header(
     *              header="Time-Zone",
     *              description="Zona horaria ",
     *              @OA\Schema( type="string" )
     *         ),

     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                  property="res",
     *                  type="boolean",
     *                  example="true"
     *             ),
     *             @OA\Property(
     *                  property="msg",
     *                  type="string",
     *                  example="OK"
     *             ),
     *             @OA\Property(
     *                property="data",
     *                type="array",
     *                @OA\Items(
     *                      @OA\Property(
     *                         property="id",
     *                         type="string",
     *                         example="1"
     *                      ),
     *                      @OA\Property(
     *                         property="title",
     *                         type="string",
     *                         example="ONCE"
     *                      ),
     *                      @OA\Property(
     *                         property="parent_id",
     *                         type="number",
     *                         example="2"
     *                      ),
     *                      @OA\Property(
     *                         property="created_at",
     *                         type="string",
     *                         example="2022-11-27T01:29:15.000000Z"
     *                      ),
     *                      @OA\Property(
     *                         property="updated_at",
     *                         type="string",
     *                         example="2022-11-27T01:29:15.000000Z"
     *                      ),
     *                ),

     *             ),
     *        ),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Error",

     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                  property="res",
     *                  type="boolean",
     *                  example="false"
     *             ),
     *             @OA\Property(
     *                  property="msg",
     *                  type="string",
     *                  example="El Nodo no existe"
     *             ),
     *        ),
     *     ),
     *      @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                  property="res",
     *                  type="boolean",
     *                  example="false"
     *             ),
     *             @OA\Property(
     *                  property="msg",
     *                  type="string",
     *                  example="El Nodo no es padre"
     *             ),
     *        ),
     *     ),
     * )
     */
    public function getChildrenByParent(Request $request,$parent_id)
    {
        try {
            $n = Nodo::find($parent_id);
            if(empty($n)){
                return response()->json([
                    'res' => false,
                    'msg' => 'El Nodo no existe',
                ], 404);
            }

            if($n->childrenNodo->count() == 0){
                return response()->json([
                    'res' => false,
                    'msg' => 'El Nodo no es padre',
                ], 400);
            }

            $nodos_translated =$this->headerParams($request, $n->childrenNodo);
            return response()->json([
                'res' => true,
                'msg' => 'Ok',
                'data' => $nodos_translated
            ], 200);


        } catch (\Throwable $th) {
            return response()->json([
                'res' => false,
                'msg' => 'Error',
                'data' => $th
            ], 400);
        }
    }

/**
     * @OA\Get(
     *     path="/api/nodo/getParents",
     *     description="Listar nodos padres",
     *
     *     @OA\Response(
     *         @OA\Header(
     *              header="Accept-Language",
     *              description="Idioma en formato ISO 639-1 ",
     *              @OA\Schema( type="string" ),
     *         ),
     *         @OA\Header(
     *              header="Time-Zone",
     *              description="Zona horaria ",
     *              @OA\Schema( type="string" )
     *         ),

     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                  property="res",
     *                  type="boolean",
     *                  example="true"
     *             ),
     *             @OA\Property(
     *                  property="msg",
     *                  type="string",
     *                  example="OK"
     *             ),
     *             @OA\Property(
     *                property="data",
     *                type="array",
     *                @OA\Items(
     *                      @OA\Property(
     *                         property="id",
     *                         type="string",
     *                         example="1"
     *                      ),
     *                      @OA\Property(
     *                         property="title",
     *                         type="string",
     *                         example="ONCE"
     *                      ),
     *                      @OA\Property(
     *                         property="parent_id",
     *                         type="number",
     *                         example="2"
     *                      ),
     *                      @OA\Property(
     *                         property="created_at",
     *                         type="string",
     *                         example="2022-11-27T01:29:15.000000Z"
     *                      ),
     *                      @OA\Property(
     *                         property="updated_at",
     *                         type="string",
     *                         example="2022-11-27T01:29:15.000000Z"
     *                      ),
     *                ),

     *             ),
     *        ),
     *     ),
     *      @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                  property="res",
     *                  type="boolean",
     *                  example="false"
     *             ),
     *             @OA\Property(
     *                  property="msg",
     *                  type="string",
     *                  example="Error"
     *             ),
     *        ),
     *     ),
     * )
     */
    public function getParents(Request $request)
    {
        try {
            $nodos = Nodo::all();
            $nodos_parents = [];
            foreach ($nodos as $key => $nodo) {
                if (!empty($nodo->parentNodo)) {
                    $exist = in_array($nodo->parentNodo, $nodos_parents);
                    if (!$exist) {
                        array_push($nodos_parents, $nodo->parentNodo);
                    }
                }
            }

            $nodos_translated =$this->headerParams($request, $nodos_parents);

            return response()->json([
                'res' => true,
                'msg' => 'Ok',
                'data' => $nodos_translated
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'res' => false,
                'msg' => 'Error',
                'data' => $th
            ], 400);
        }
    }





    public function headerParams(Request $request, $nodos)
    {
        $t = new NumberFormatter('es', NumberFormatter::SPELLOUT);
        if ($request->hasHeader("Accept-Language")) {
            $locale = $request->header("Accept-Language");
            $t = new NumberFormatter($locale, NumberFormatter::SPELLOUT);
        }
        $nodo_translated = [];

        foreach ($nodos as $key => $nodo) {
            $nodo->title = $t->format($nodo->id);
            $nodo->created_at = $nodo->created_at->setTimezone($request->header('Time-Zone'));
            array_push($nodo_translated, $nodo);
        }

        return $nodo_translated;
    }



}
