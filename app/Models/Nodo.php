<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nodo extends Model
{
    use HasFactory;
    const UPDATED_AT = null;
    protected $fillable = [
        'parent_id',
        'title',
        'created_at',
    ];

    public function parentNodo(){
        return $this->hasOne(Nodo::class, 'id', 'parent_id');
    }

    public function childrenNodo(){
        return $this->hasMany(Nodo::class, 'parent_id', 'id');
    }
}
