<?php

namespace Database\Seeders;

use App\Models\Nodo;
use Carbon\Carbon;
use Faker\Core\Number;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NodoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nodos')->insert([
        	[
                'id' => 1,
	        	'parent_id' => null,
	        	'title' => 'uno',
                'created_at' => Carbon::parse(date('Y-m-d H:i:s'))->format('Y-m-d H:i:s')
        	],
            [
	        	'id' => 2,
	        	'parent_id' => null,
	        	'title' => 'dos',
                'created_at' => Carbon::parse(date('Y-m-d H:i:s'))->format('Y-m-d H:i:s')
        	],
            [
	        	'id' => 3,
	        	'parent_id' => 2,
	        	'title' => 'tres',
                'created_at' => Carbon::parse(date('Y-m-d H:i:s'))->format('Y-m-d H:i:s')
        	],
            [
	        	'id' => 4,
	        	'parent_id' => 2,
	        	'title' => 'cuatro',
                'created_at' => Carbon::parse(date('Y-m-d H:i:s'))->format('Y-m-d H:i:s')
        	],
            [
	        	'id' => 5,
	        	'parent_id' => 1,
	        	'title' => 'cinco',
                'created_at' => Carbon::parse(date('Y-m-d H:i:s'))->format('Y-m-d H:i:s')
        	],
            [
	        	'id' => 6,
	        	'parent_id' => null,
	        	'title' => 'seis',
                'created_at' => Carbon::parse(date('Y-m-d H:i:s'))->format('Y-m-d H:i:s')
        	]

        ]);
    }
}
